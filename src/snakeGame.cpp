#include <SFML/Graphics.hpp>
#include <array>
#include <random>
#include <iostream>

namespace snake {
    enum direction {UP, DOWN, LEFT, RIGHT};
    enum item {
        EMPTY = -1,
        PLUS_ONE = -2,
        WALL = -3,
    };

    const unsigned int width = 40;
    const unsigned int height = 30;
    const unsigned int initialLength = 10;
    int board[width][height]{};

    int headPosition[2]{};
    direction currentDirection{};
    int snakeLength{};

    void initialize();
    void setNewHeadPosition();
    void setRandomOnEmpty(item);
    void update();
    sf::Uint8* representation();
}

void snake::initialize()
{
    for (int x{0}; x < width; x++)
    {
        for (int y{0}; y < height; y++)
        {
            board[x][y] = -1;
        }
    }

    headPosition[0] = width/2;
    headPosition[1] = height/2;

    snakeLength = initialLength;
    setRandomOnEmpty(PLUS_ONE);
    for (int i{0}; i < 10; i++)
    {
        setRandomOnEmpty(WALL);
    }
}

void snake::setNewHeadPosition()
{
    if (currentDirection == UP)
        headPosition[1]--;
    else if (currentDirection == DOWN)
        headPosition[1]++;
    else if (currentDirection == LEFT)
        headPosition[0]--;
    else if (currentDirection == RIGHT)
        headPosition[0]++;

    if (headPosition[0] < 0)
        headPosition[0] = 0;
    if (headPosition[1] < 0)
        headPosition[1] = 0;
    if (headPosition[0] >= width)
        headPosition[0] = width-1;
    if (headPosition[1] >= height)
        headPosition[1] = height-1;

    int board_value = board[headPosition[0]][headPosition[1]];
    if (board_value == PLUS_ONE)
    {
        snakeLength++;
        setRandomOnEmpty(PLUS_ONE);
        setRandomOnEmpty(WALL);
    }
    if (board_value >= 0 or board_value == WALL)
    {
        std::cout << "Score: " << snakeLength-initialLength << std::endl;
        initialize();
    }

    board[headPosition[0]][headPosition[1]] = 0;
}

void snake::setRandomOnEmpty(item it)
{
    std::random_device rd;
    std::mt19937 rng(rd());
    std::uniform_int_distribution<> dist_x(0, width-1);
    std::uniform_int_distribution<> dist_y(0, height-1);

    while (true)
    {
        int x = dist_x(rng);
        int y = dist_y(rng);
        if (board[x][y] == EMPTY)
        {
            board[x][y] = it;
            break;
        }
    }
}

void snake::update()
{
    setNewHeadPosition();

    for (int x{0}; x < width; x++)
    {
        for (int y{0}; y < height; y++)
        {
            if (board[x][y] >= snakeLength)
            {
                board[x][y] = -1;
            }
            else if (board[x][y] >= 0)
            {
                board[x][y]++;
            }
        }
    }
}

sf::Uint8* snake::representation() {
    // last index is RGBA
    auto repr {new sf::Uint8[width*height*4]{}};

    for (int x{0}; x < width; x++)
    {
        for (int y{0}; y < height; y++)
        {
            int baseIndex = 4*(x + y*width);
            if (board[x][y] >= 0) {
                int value = 55 + 200 * (1 - (float) board[x][y] / snakeLength);
                repr[baseIndex + 0] = 0; // red
                repr[baseIndex + 1] = value; // green
                repr[baseIndex + 2] = value; // blue
            } else if (board[x][y] == EMPTY) {
                repr[baseIndex + 0] = 0; // red
                repr[baseIndex + 1] = 0; // green
                repr[baseIndex + 2] = 0; // blue
            } else if (board[x][y] == PLUS_ONE) {
                repr[baseIndex + 0] = 255; // red
                repr[baseIndex + 1] = 0; // green
                repr[baseIndex + 2] = 0; // blue
            } else if (board[x][y] == WALL) {
                repr[baseIndex + 0] = 100; // red
                repr[baseIndex + 1] = 100; // green
                repr[baseIndex + 2] = 100; // blue
            }
            repr[baseIndex + 3] = 255; // alpha
        }
    }
    return &repr[0];
}
