#include <SFML/Graphics.hpp>

#include "snakeGame.cpp"

int main()
{
    sf::RenderWindow window(
            sf::VideoMode(1200, 900),
            "Snake Clone",
            sf::Style::Titlebar | sf::Style::Close
            );

    window.setFramerateLimit(10);

    snake::initialize();

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            switch (event.type) {
                case sf::Event::Closed : {
                    window.close();
                    break; }
                case sf::Event::KeyPressed : {
                    if (event.key.code == sf::Keyboard::W
                            or event.key.code == sf::Keyboard::Up)
                        snake::currentDirection = snake::UP;
                    if (event.key.code == sf::Keyboard::A
                            or event.key.code == sf::Keyboard::Left)
                        snake::currentDirection = snake::LEFT;
                    if (event.key.code == sf::Keyboard::S
                            or event.key.code == sf::Keyboard::Down)
                        snake::currentDirection = snake::DOWN;
                    if (event.key.code == sf::Keyboard::D
                            or event.key.code == sf::Keyboard::Right)
                        snake::currentDirection = snake::RIGHT;
                    break; }
            }
        }

        snake::update();

        window.clear(sf::Color::Black);

        sf::Texture t_field;
        t_field.create(snake::width, snake::height);
        t_field.update(snake::representation());

        sf::Sprite s_field(t_field);
        s_field.scale(1200/snake::width, 900/snake::height);

        window.draw(s_field);
        window.display();
    }

    return 0;
}
